import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Counter from ".";

describe("Counter Component", () => {
  describe("Verificando existencia dos componentes e className", () => {
    test("Deve iniciar o titulo com o valor 0", () => {
      render(<Counter />);

      const counterTitle = screen.getByText("0");

      expect(counterTitle).toBeInTheDocument();
    });

    test("Deve conter a classe counter__title no titulo", () => {
      render(<Counter />);

      const counterTitle = screen.getByText("0");

      expect(counterTitle).toHaveClass("counter__title");
    });

    test("Não deve iniciar o titulo com as classes counter__title--increment e counter__title--decrement", () => {
      render(<Counter />);

      const counterTitle = screen.getByText("0");

      expect(counterTitle).not.toHaveClass("counter__title--increment");
      expect(counterTitle).not.toHaveClass("counter__title--decrement");
    });

    test("Deve conter um botão incrementar", () => {
      render(<Counter />);

      const buttonIncrement = screen.getByRole("button", {
        name: /incrementar/i,
      });

      expect(buttonIncrement).toBeInTheDocument();
    });

    test("Deve conter as classes button e button--increment no botão incrementar", () => {
      render(<Counter />);

      const buttonIncrement = screen.getByRole("button", {
        name: /incrementar/i,
      });

      expect(buttonIncrement).toHaveClass("button");
      expect(buttonIncrement).toHaveClass("button--increment");
    });

    test("Deve conter um botão decrementar", () => {
      render(<Counter />);

      const buttonDecrement = screen.getByRole("button", {
        name: /decrementar/i,
      });

      expect(buttonDecrement).toBeInTheDocument();
    });

    test("Deve conter as classes button e button--decrement no botão decrementar", () => {
      render(<Counter />);

      const buttonDecrement = screen.getByRole("button", {
        name: /decrementar/i,
      });

      expect(buttonDecrement).toHaveClass("button");
      expect(buttonDecrement).toHaveClass("button--decrement");
    });
  });

  describe("Testes de Eventos e Funções", () => {
    test("Deve incrementar + 1 ao clicar no botão incrementar", () => {
      render(<Counter />);

      const buttonIncrement = screen.getByRole("button", {
        name: /incrementar/i,
      });

      expect(screen.queryByText("1")).toBeNull();
      userEvent.click(buttonIncrement);
      expect(screen.getByText("1")).toBeInTheDocument();
    });

    test("Deve decrementar - 1 ao clicar no botão decrementar", () => {
      render(<Counter />);

      const buttonDecrement = screen.getByRole("button", {
        name: /decrementar/i,
      });

      expect(screen.queryByText("-1")).toBeNull();
      userEvent.click(buttonDecrement);
      expect(screen.getByText("-1")).toBeInTheDocument();
    });

    test("Deve adicionar a classe counter__title--increment no titulo, quando o valor for maior do que 0", () => {
      render(<Counter />);

      const buttonIncrement = screen.getByRole("button", {
        name: /incrementar/i,
      });

      expect(screen.queryByText("0")).not.toHaveClass(
        "counter__title--increment"
      );
      userEvent.click(buttonIncrement);
      expect(screen.getByText("1")).toHaveClass("counter__title--increment");
    });

    test("Deve adicionar a classe counter__title--decrement no titulo, quando o valor for menor do que 0", () => {
      render(<Counter />);

      const buttonDecrement = screen.getByRole("button", {
        name: /decrementar/i,
      });

      expect(screen.queryByText("0")).not.toHaveClass(
        "counter__title--decrement"
      );
      userEvent.click(buttonDecrement);
      expect(screen.getByText("-1")).toHaveClass("counter__title--decrement");
    });
  });
});
